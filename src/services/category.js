import ApiService from './base';

export const getAllCategory = async () => {
  const url = `/getAllCategory`
  return ApiService.post(url)
}