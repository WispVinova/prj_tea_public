import ApiService from './base';


export const getProduct = async (id) => {
  const url = `/getProduct`
  return ApiService.post(url, {id})
}


export const getByCategory = async (data) => {
  const url = `/getByCategory`
  return ApiService.post(url, data)
}

export const getAllProduct = async () => {
  const url = `/getAllProduct`
  return ApiService.post(url)
}

export const getBestSelling = async () => {
  const url = `/getBestSelling`
  return ApiService.post(url)
}
