import ApiService from './base';


export const addCart = async (data) => {

  data = {
    ...data,
    uid: getUid()
  }

  const url = `/addCart`
  return ApiService.post(url, data)
}


export const getCartUser = async () => {

  const uid =  getUid()

  const url = `/getCartUser`
  return ApiService.post(url, {uid})
}

export const getCart = async (id) => {
  const url = `/getCart`
  return ApiService.post(url, {id})
}



const getUid = () => {
  let uid = null;
  try{
    const token = JSON.parse(localStorage.getItem("TOKEN_PL")) || {};
    uid = token.localId || null;
  }catch(e){console(e)}
  return uid;
}
