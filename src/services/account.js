import ApiService from './base';

export const Login = async data => {
  data = {
    ...data,
    returnSecureToken: true
  }
  const url = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyA7Qn7AWRZyr_VEzeoDej2nG1hQPbk5in4`
  return ApiService.post(url, data)
}

export const Register = async data => {
  const url = `/registerNewUser`
  return ApiService.post(url, data)
}

export const getUserData = async () => {
  let uid = getUid()
  const url = `/getUserData`
  return ApiService.post(url, {uid})
}

export const updateUserData = async (data) => {
  const url = `/updateUserData`
  return ApiService.post(url, data)
}




const getUid = () => {
  let uid = null;
  try{
    const token = JSON.parse(localStorage.getItem("TOKEN_PL")) || {};
    uid = token.localId || null;
  }catch(e){console(e)}
  return uid;
}
