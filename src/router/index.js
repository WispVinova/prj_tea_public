import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

  const routes = [
    {
      path: '/',
      name: 'Home',
      component: () => import("../views/Home/Home.vue"),
    },

    {
      path: '/san-pham',
      name: 'Products',
      component: () => import("@/views/Products/Products.vue"),
    },

    {
      path: '/ve-chung-toi',
      name: 'About',
      component: () => import("@/views/About.vue"),
    },

    {
      path: '/dang-nhap',
      name: 'Login',
      component: () => import("@/views/Login/Login.vue"),
    },

    {
      path: '/lich-su-mua-hang',
      name: 'OrderHistory',
      meta: {
        login: true,
      },
      component: () => import("@/views/OrderHistory.vue"),
    },

    {
      path: '/quen-mat-khau',
      name: 'ForgotPassword',
      component: () => import("@/views/ForgotPassword.vue"),
    },

    {
      path: '/thanh-toan',
      name: 'Payment',
      component: () => import("@/views/Payment.vue"),
    },

    {
      path: '/gio-hang',
      name: 'Cart',
      component: () => import("@/views/Cart.vue"),
    },

    {
      path: '/trang-ca-nhan',
      name: 'UserProfile',
      meta: {
        login: true,
      },
      component: () => import("@/views/UserProfile.vue"),
    },

    {
      path: '/chi-tiet-san-pham/:id',
      name: 'ProductDetail',
      component: () => import("@/views/ProductDetail.vue"),
    },
    {
      path: '/detail-order/:id',
      name: 'detailorder',
      component: () => import("@/views/DetailOrder.vue"),
    },
  ]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  console.log(to)
  if(to.meta.login){
    let token = null;
    try{
      token = JSON.parse(localStorage.getItem("TOKEN_PL"))
    }catch(e){console.log(e)}
    if(!token){
      return  next({name: 'Login'})
    }
  }
  next() 
});

export default router
