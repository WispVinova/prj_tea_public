import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';

import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';

import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-default.css';

import Vuelidate from 'vuelidate';

import  ApiService  from '@/services/base';


Vue.config.productionTip = false;

Vue.use(Vuelidate)

Vue.use(VueToast, {
  position: 'top',
  duration: 2 * 1000
})

Vue.use(Loading, {color: '#1428A0'}) // Primary color

ApiService.init();

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
